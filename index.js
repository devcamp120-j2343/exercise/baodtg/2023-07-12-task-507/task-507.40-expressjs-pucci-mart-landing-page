//khởi tạo express
const express = require('express');
const path = require('path')

//Khởi tạo expres app
const app = express();

//Khởi tạo port
const port = 8000;

app.use(express.static(__dirname + "/views"));

//Khai báo api dạng Get('/') sẽ chạy vào đây
app.get('/', (req, res) => {
    console.log(__dirname);
    
    res.sendFile(path.join(__dirname + "/views/landingPage.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);

})